package br.com.itau.troca_de_livros.helpers;

import java.util.Optional;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;

public class JwtHelper {
	private static final String chave = "aaaaavaaaaaErrooooouuu";

	public static Optional<String> gerar(int idUsuario) {
		try {
			Algorithm algoritmo = Algorithm.HMAC256(chave);
			String token = JWT.create().withClaim("idUsuario", idUsuario).sign(algoritmo);

			return Optional.of(token);
		} catch (Exception exception) {
			return Optional.empty();
		}
	}

	public static Optional<Integer> verificar(String token){
		try{
			Algorithm algoritmo = Algorithm.HMAC256(chave);
		
			JWTVerifier verificador = JWT.require(algoritmo).build();
		
			DecodedJWT tokenDecodificado = verificador.verify(token);
		
			Integer idUsuario = tokenDecodificado.getClaim("idUsuario").asInt();
		
			return Optional.of(idUsuario);
		}catch (Exception e) {
			return Optional.empty();
		}
	}
}