package br.com.itau.troca_de_livros.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import br.com.itau.troca_de_livros.models.Livro;

public interface LivroRepository extends CrudRepository<Livro, Integer> {

	@Query(value = "SELECT * FROM trocalivros.livros WHERE USUARIO_ID != ?1", nativeQuery = true)
	public Iterable<Livro> findAllByCatalogo(int id);

	@Query(value = "SELECT * FROM trocalivros.livros WHERE USUARIO_ID = ?1", nativeQuery = true)
	public Iterable<Livro> findAllByCatalogoUsuario(int id);

	@Query(value = "SELECT * FROM trocalivros.livros WHERE titulo like CONCAT(:titulo,'%')", nativeQuery = true)
	public Iterable<Livro> findAllByTitulo(@Param("titulo") String titulo);

}