package br.com.itau.troca_de_livros.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.troca_de_livros.models.Autor;

public interface AutorRepository extends CrudRepository<Autor, Integer>{

}
