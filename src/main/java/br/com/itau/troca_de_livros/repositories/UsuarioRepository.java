package br.com.itau.troca_de_livros.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.troca_de_livros.models.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer>{

	public Iterable<Usuario> findAllByNome(String nome);
}
