package br.com.itau.troca_de_livros.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import br.com.itau.troca_de_livros.models.Login;

public interface LoginRepository extends CrudRepository<Login, Integer> {

	public Optional<Login> findByLogin(String login);

	@Query(value = "select * from trocalivros.login where usuario_id = ?1", nativeQuery = true)
	public Optional<Login> findByIdUsuario(int usuario_id);
}
