package br.com.itau.troca_de_livros.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.troca_de_livros.models.Livro;
import br.com.itau.troca_de_livros.services.LivroService;

@RestController
@RequestMapping("/livros")
@CrossOrigin
public class LivroController {

	@Autowired
	LivroService livroService;

	@GetMapping
	public Iterable<Livro> listarLivros() {
		return livroService.buscarTodos();
	}

	@GetMapping("/catalogo/{id}")
	public Iterable<Livro> listarCatalogo(@PathVariable int id) {
		return livroService.buscarCatalogo(id);
	}

	@GetMapping("/catalogousuario/{id}")
	public Iterable<Livro> listarCatalogoUsuario(@PathVariable int id) {
		return livroService.buscarCatalogoUsuario(id);
	}

	@GetMapping("/{titulo}")
	public Iterable<Livro> buscarLivroPorTitulo(@PathVariable String titulo) {
		return livroService.obterLivroPorTitulo(titulo);
	}

	@PostMapping
	public void inserirLivros(@RequestBody Livro livro) {
		livroService.inserir(livro);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity criarLivro(@PathVariable int id) {
		boolean resultadoDelecao = livroService.deletarLivro(id);

		if (resultadoDelecao) {
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.badRequest().body("Livro não existe!");
	}

	@PatchMapping("/{id}")
	public ResponseEntity<?> alterarLivro(@PathVariable int id, @RequestBody Livro livro) {
		boolean deuCerto = livroService.alterar(id, livro);

		if (!deuCerto) {
			return ResponseEntity.status(400).build();
		}

		return ResponseEntity.status(200).build();
	}

	@ExceptionHandler(HttpMessageNotReadableException.class)
	public ResponseEntity tratarExcessao() {
		return ResponseEntity.status(400).body("Categoria não permitida!");
	}
}