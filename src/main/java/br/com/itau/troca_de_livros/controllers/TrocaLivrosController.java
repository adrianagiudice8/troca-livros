package br.com.itau.troca_de_livros.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.troca_de_livros.inout.TrocaEntrada;
import br.com.itau.troca_de_livros.services.TrocaService;

@RestController
@RequestMapping("/trocalivros")
public class TrocaLivrosController {

	@Autowired
	TrocaService trocaService;

	@PutMapping
	public ResponseEntity<?> trocarLivros(@RequestBody TrocaEntrada trocaEntrada) {
		boolean deuCerto = trocaService.trocarLivros(trocaEntrada);

		if (!deuCerto) {
			return ResponseEntity.status(400).build();
		}

		return ResponseEntity.status(200).build();
	}

}
