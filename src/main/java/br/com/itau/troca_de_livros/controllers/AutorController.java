package br.com.itau.troca_de_livros.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.troca_de_livros.models.Autor;
import br.com.itau.troca_de_livros.repositories.AutorRepository;
import br.com.itau.troca_de_livros.services.AutorService;

@RestController
@RequestMapping("/autor")
public class AutorController {

	@Autowired
	AutorRepository autorRepository;

	@Autowired
	AutorService autorService;

	@GetMapping
	public Iterable<Autor> listarAutor() {
		return autorService.obterAutor();
	}

	@GetMapping("/{id}")
	public Optional<Autor> listarAutorPorId(@PathVariable int id) {
		return autorService.obterAutorPorId(id);
	}
	
	@PostMapping
	public ResponseEntity<?> inserirAutor(@RequestBody Autor autor) {
		boolean deuCerto = autorService.inserir(autor);

		if (!deuCerto) {
			return ResponseEntity.status(400).build();
		}

		return ResponseEntity.status(201).build();
	}

	@PatchMapping("/{id}")
	public ResponseEntity<?> alterarAutor(@PathVariable int id, @RequestBody Autor autor) {
		boolean deuCerto = autorService.alterar(id, autor);

		if (!deuCerto) {
			return ResponseEntity.status(400).build();
		}

		return ResponseEntity.status(200).build();
	}
}
