package br.com.itau.troca_de_livros.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.troca_de_livros.helpers.JwtHelper;
import br.com.itau.troca_de_livros.models.Login;
import br.com.itau.troca_de_livros.services.LoginService;

@CrossOrigin
@RestController
@RequestMapping("/login") 
public class LoginController {

	@Autowired
	LoginService loginService;

	@PostMapping 
	public ResponseEntity fazerLogin(@RequestBody Login login) {
		Optional<Login> loginOptional = loginService.buscarPorLoginESenha(login.getLogin(), login.getSenha());

		if (loginOptional.isPresent()) {
			Optional<String> tokenOptional = JwtHelper.gerar(loginOptional.get().getIdUsuario());

			return ResponseEntity.ok(tokenOptional);
		}

		return ResponseEntity.status(400).build();
	}

	@GetMapping("/{token}")
	public Optional<Integer> testeVerificar(@PathVariable String token) {
		return JwtHelper.verificar(token);
	}


	@DeleteMapping("/{id}")
	public ResponseEntity deletarLogin(@PathVariable int id) {
		boolean resultadoDelecao = loginService.deletarLogin(id);
		
		if (resultadoDelecao) {
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.badRequest().build();
	}
}
