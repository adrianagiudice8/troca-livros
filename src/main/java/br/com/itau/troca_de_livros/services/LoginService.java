package br.com.itau.troca_de_livros.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.itau.troca_de_livros.models.Login;
import br.com.itau.troca_de_livros.repositories.LoginRepository;

@Service
public class LoginService {

	@Autowired
	LoginRepository loginRepository;
	
	BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
	 
	public Optional<Login> buscarPorLoginESenha(String login, String senha) {
		Optional<Login> loginOptional = loginRepository.findByLogin(login);

		if (loginOptional.isPresent()) {
			Login loginUsuario = loginOptional.get();
			
			if (encoder.matches(senha, loginUsuario.getSenha())) {
				return loginOptional;
			}
		}
		return Optional.empty();
	}
	
	private Optional<Login> obterLoginPorId(int id){
		return loginRepository.findById(id);
	}
	
	public boolean deletarLogin(int id) {
		Optional<Login> loginOptional= obterLoginPorId(id);
		
		if (loginOptional.isPresent()) { 
			loginRepository.delete(loginOptional.get());
			return true;
		}
		return false;
	}

}
