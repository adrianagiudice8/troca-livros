package br.com.itau.troca_de_livros.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.troca_de_livros.models.Autor;
import br.com.itau.troca_de_livros.repositories.AutorRepository;

@Service
public class AutorService {

	@Autowired
	AutorRepository autorRepository;

	public Iterable<Autor> obterAutor() {
		return autorRepository.findAll();
	}

	public Optional<Autor> obterAutorPorId(int id) {
		return autorRepository.findById(id);
	}

	public boolean inserir(Autor autor) {
		autorRepository.save(autor);
		return true;
	}
	
	public boolean alterar(int id, Autor autor) {
		
		Optional<Autor> autorOptional = autorRepository.findById(id);

		if (autorOptional.isPresent()) {
			autor = mesclarAtributos(autor, autorOptional.get());
			autorRepository.save(autor);
			return true;
		}

		return false;
	}
	
	private Autor mesclarAtributos(Autor novo, Autor antigo) {
		if (novo.getNome() != null && !novo.getNome().isEmpty()) {
			antigo.setNome(novo.getNome());
		}

		return antigo;
	}

}
