package br.com.itau.troca_de_livros.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.troca_de_livros.models.Livro;
import br.com.itau.troca_de_livros.repositories.LivroRepository;

@Service
public class LivroService {

	@Autowired
	LivroRepository livroRepository;

	public Iterable<Livro> buscarTodos() {
		return livroRepository.findAll();
	}

	public Iterable<Livro> buscarCatalogo(int id) {
		return livroRepository.findAllByCatalogo(id);
	}

	public Iterable<Livro> buscarCatalogoUsuario(int id) {
		return livroRepository.findAllByCatalogoUsuario(id);
	}

	public Optional<Livro> obterLivroPorId(int id) {
		return livroRepository.findById(id);
	}

	public Iterable<Livro> obterLivroPorTitulo(String titulo) {
		return livroRepository.findAllByTitulo(titulo);
	}

	public boolean inserir(Livro livros) {
		livroRepository.save(livros);
		return true;
	}

	public boolean alterar(int id, Livro livro) {

		Optional<Livro> livroOptional = livroRepository.findById(id);

		if (livroOptional.isPresent()) {
			livro = mesclarAtributos(livro, livroOptional.get());
			livroRepository.save(livro);
			return true;
		}

		return false;
	}

	private Livro mesclarAtributos(Livro novo, Livro antigo) {
		if (novo.getTitulo() != null && !novo.getTitulo().isEmpty()) {
			antigo.setTitulo(novo.getTitulo());
		}

		if (novo.getAutor() != null) {
			antigo.setAutor(novo.getAutor());
		}
		
		if (novo.getCategoria() != null) {
			antigo.setCategoria(novo.getCategoria());
		}

		antigo.setDisponivel(1);

		return antigo;
	}

	public boolean deletarLivro(int id) {
		Optional<Livro> livroOptional = obterLivroPorId(id);

		if (livroOptional.isPresent()) {
			livroRepository.delete(livroOptional.get());
			return true;
		}
		return false;
	}

}