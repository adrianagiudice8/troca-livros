package br.com.itau.troca_de_livros.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.troca_de_livros.inout.TrocaEntrada;
import br.com.itau.troca_de_livros.models.Livro;
import br.com.itau.troca_de_livros.models.Usuario;
import br.com.itau.troca_de_livros.repositories.LivroRepository;
import br.com.itau.troca_de_livros.repositories.UsuarioRepository;

@Service
public class TrocaService {

	@Autowired
	LivroRepository livroRepository;
	
	@Autowired
	UsuarioRepository usuarioRepository;

	public boolean trocarLivros(TrocaEntrada trocaEntrada) {
		if (trocaEntrada.getUsuario_idEnviado() != 0
				&& trocaEntrada.getUsuario_idEnviado() != trocaEntrada.getUsuario_idSolicitado()) {

			trocar(trocaEntrada.getIdEnviado(), trocaEntrada.getUsuario_idSolicitado());
			trocar(trocaEntrada.getIdSolicitado(), trocaEntrada.getUsuario_idEnviado());
			
			return true;
		}
		return false;
	}

	private void trocar(int idLivro, int idUsuario) {

		Optional<Livro> livrosOptional = livroRepository.findById(idLivro);
		Optional<Usuario> usuarioOptional = usuarioRepository.findById(idUsuario);
		
		if (livrosOptional.isPresent() && usuarioOptional.isPresent()) {
			Livro livro = livrosOptional.get();
			Usuario usuario = usuarioOptional.get();

			livro.setUsuario(usuario);
			livro.setDisponivel(0);

			livroRepository.save(livro);
		}

	}
}