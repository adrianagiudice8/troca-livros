package br.com.itau.troca_de_livros.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.itau.troca_de_livros.inout.EntradaSaidaUsuario;
import br.com.itau.troca_de_livros.models.Login;
import br.com.itau.troca_de_livros.models.Usuario;
import br.com.itau.troca_de_livros.repositories.LoginRepository;
import br.com.itau.troca_de_livros.repositories.UsuarioRepository;

@Service
public class UsuarioService {

	@Autowired
	UsuarioRepository usuarioRepository;

	@Autowired
	LoginRepository loginRepository;

	BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

	private Usuario usuario = new Usuario();
	private Login login = new Login();

	public Iterable<Usuario> obterUsuario() {
		return usuarioRepository.findAll();
	}

	public Iterable<Usuario> obterUsuarioPorNome(String nome) {
		return usuarioRepository.findAllByNome(nome);
	}

	private Optional<Usuario> obterUsuarioPorId(int id) {
		return usuarioRepository.findById(id);
	}

	public boolean inserir(EntradaSaidaUsuario usuarioDados) {
		inserirUsuario(usuarioDados);
		return true;
	}

	private void inserirUsuario(EntradaSaidaUsuario usuarioDados) {
		usuario.setEmail(usuarioDados.getEmail());
		usuario.setNome(usuarioDados.getNome());

		usuarioRepository.save(usuario);

		login.setIdUsuario(usuario.getId());
		login.setLogin(usuarioDados.getLogin());

		String hash = encoder.encode(usuarioDados.getSenha());

		login.setSenha(hash);

		loginRepository.save(login);
	}

	public boolean alterarUsuario(int id, Usuario usuario) {
		Optional<Usuario> usuarioOptional = obterUsuarioPorId(id);

		usuario.setId(id);

		if (usuarioOptional.isPresent()) {
			usuarioRepository.save(usuario);
			return true;
		}
		return false;
	}

	private Optional<Login> obterLoginPorId(int id) {
		return loginRepository.findByIdUsuario(id);
	}

	public boolean deletarUsuario(int id) {
		Optional<Login> loginOptional = obterLoginPorId(id);
		Optional<Usuario> usuarioOptional = obterUsuarioPorId(id);

		if (usuarioOptional.isPresent()) {
			loginRepository.delete(loginOptional.get());
			usuarioRepository.delete(usuarioOptional.get());
			return true;
		}
		return false;
	}
}
