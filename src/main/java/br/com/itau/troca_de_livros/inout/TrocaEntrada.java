package br.com.itau.troca_de_livros.inout;

public class TrocaEntrada {

	private int idSolicitado;
	private int usuario_idSolicitado;
	private int idEnviado;
	private int usuario_idEnviado;
	
	public int getIdSolicitado() {
		return idSolicitado;
	}
	public void setIdSolicitado(int idSolicitado) {
		this.idSolicitado = idSolicitado;
	}
	public int getUsuario_idSolicitado() {
		return usuario_idSolicitado;
	}
	public void setUsuario_idSolicitado(int usuario_idSolicitado) {
		this.usuario_idSolicitado = usuario_idSolicitado;
	}
	public int getIdEnviado() {
		return idEnviado;
	}
	public void setIdEnviado(int idEnviado) {
		this.idEnviado = idEnviado;
	}
	public int getUsuario_idEnviado() {
		return usuario_idEnviado;
	}
	public void setUsuario_idEnviado(int usuario_idEnviado) {
		this.usuario_idEnviado = usuario_idEnviado;
	}

}
