CREATE TABLE `livros` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`titulo` VARCHAR(255) NOT NULL,
	`categoria` VARCHAR(255) NOT NULL,
	`autor_id` INT(11) NOT NULL,
	`usuario_id` INT(11) NOT NULL,
	`disponivel` TINYINT(1) NOT NULL,
	PRIMARY KEY (`id`),
	FOREIGN KEY (`autor_id`) REFERENCES autores(`id`),
	FOREIGN KEY (`usuario_id`) REFERENCES usuarios(`id`)
);