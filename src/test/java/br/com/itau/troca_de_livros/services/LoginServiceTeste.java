package br.com.itau.troca_de_livros.services;

import static org.junit.Assert.assertNotNull;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import br.com.itau.troca_de_livros.models.Login;
import br.com.itau.troca_de_livros.repositories.LoginRepository;


public class LoginServiceTeste {

	LoginRepository loginRepository;
	LoginService loginService;
	
	Login login;
	
	@Before
	public void prepararTeste() {
		loginService = new LoginService();
		loginRepository = Mockito.mock(LoginRepository.class);
		
		loginService.loginRepository = loginRepository;
		
		login = new Login();
		login.setId(1);
		login.setLogin("teste");
		login.setSenha("senha123");
	}

	@Test
	public void testarLogarVazio() {
		
		Mockito
			.when(loginRepository.findByLogin(login.getLogin()))
			.thenReturn(Optional.of(login));
		
		Optional<Login> loginRetorno = loginService.buscarPorLoginESenha(login.getLogin(), login.getSenha());
		
		assertNotNull(loginRetorno); 
	}

}
