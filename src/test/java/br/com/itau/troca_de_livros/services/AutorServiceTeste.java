package br.com.itau.troca_de_livros.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import br.com.itau.troca_de_livros.models.Autor;
import br.com.itau.troca_de_livros.repositories.AutorRepository;

public class AutorServiceTeste {

	Autor autor;
	AutorService autorService;
	AutorRepository autorRepository;
	
	ArrayList<Autor> autores;

	@Before
	public void inicializar() {
		autorService = new AutorService();
		autorRepository = Mockito.mock(AutorRepository.class);
		autorService.autorRepository = autorRepository;
		autor = new Autor();
		autores = new ArrayList<>();
		autores.add(autor);
		autor.setId(1);
		autor.setNome("Erika Leonard James");
	}
	
	@Test
	public void testarInsercaoAutor() {
		Mockito.when(autorRepository.save(autor)).thenReturn(autor);
		
		boolean novoAutor = autorService.inserir(autor);
		
		assertTrue(novoAutor);

	}
	
	@Test
	public void testarErroAlteracaoAutor() {
		Mockito.when(autorRepository.save(autor)).thenReturn(autor);
		
		boolean autorNaoAlterado = autorService.alterar(autor.getId(), autor);
		
		assertFalse(autorNaoAlterado);
	}
	
	
	@Test
	public void testarAlteracaoAutor() {
		Mockito.when(autorRepository.save(autor)).thenReturn(autor);
		Mockito.when(autorRepository.findById(1)).thenReturn(Optional.of(autor));
		
		boolean autorAlterado = autorService.alterar(autor.getId(), autor);
		
		assertTrue(autorAlterado);
	}
	
	@Test
	public void testarBuscarAutores() {
		Mockito.when(autorRepository.findAll()).thenReturn(autores);
		
		ArrayList<Autor> buscarAutores = (ArrayList<Autor>) autorService.obterAutor();
		
		assertEquals(1, buscarAutores.size());
		assertEquals("Erika Leonard James", buscarAutores.get(0).getNome());
		
	}
	
	@Test
	public void testarBuscarUmAutor() {
		Mockito.when(autorRepository.findById(1)).thenReturn(Optional.of(autor));
		
		Optional<Autor> buscarAutor = autorService.obterAutorPorId(autor.getId());
		
		assertEquals("Erika Leonard James", buscarAutor.get().getNome());	

	}
}
