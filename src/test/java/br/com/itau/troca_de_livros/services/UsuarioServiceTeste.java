package br.com.itau.troca_de_livros.services;


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import br.com.itau.troca_de_livros.inout.EntradaSaidaUsuario;
import br.com.itau.troca_de_livros.models.Usuario;
import br.com.itau.troca_de_livros.repositories.UsuarioRepository;

public class UsuarioServiceTeste {

	UsuarioRepository usuarioRepository;
	
	UsuarioService usuarioService;
	
	EntradaSaidaUsuario usuarioDados;
	Usuario usuario;
	ArrayList<Usuario> usuarios;
	
	@Before
	public void prepararTeste() {
		usuarioService = new UsuarioService();
		usuarioRepository = Mockito.mock(UsuarioRepository.class);
		
		usuarioService.usuarioRepository = usuarioRepository;
		
		usuario = new Usuario();
		usuarios = new ArrayList<>();
		usuarios.add(usuario);
		usuario.setId(1);
		usuario.setNome("teste");
		usuario.setEmail("teste@teste.com");
		usuarioDados = new EntradaSaidaUsuario();

		usuarioDados.setId(1);
		usuarioDados.setNome("teste");
		usuarioDados.setEmail("teste@teste.com");
		usuarioDados.setIdUsuario(1);
		usuarioDados.setLogin("teste");
		usuarioDados.setSenha("senha123");
	}

	@Test
	public void testarInsercaoUsuario() {
		Mockito.when(usuarioRepository.save(usuario)).thenReturn(usuario);
		
		boolean usuarioNovo = usuarioService.inserir(usuarioDados);
		
		assertTrue(usuarioNovo);
	}
	
	@Test
	public void testarErroAtualizacaoUsuario() {
		Mockito.when(usuarioRepository.save(usuario)).thenReturn(usuario);
		
		boolean usuarioAtualizado = usuarioService.alterarUsuario(usuario.getId(), usuario);
		
		assertFalse(usuarioAtualizado);
	}

	@Test
	public void testarAtualizacaoUsuario() {
		Mockito.when(usuarioRepository.save(usuario)).thenReturn(usuario);
		Mockito.when(usuarioRepository.findById(1)).thenReturn(Optional.of(usuario));
		
		boolean usuarioAtualizado = usuarioService.alterarUsuario(usuario.getId(), usuario);
		
		assertTrue(usuarioAtualizado);
	}

	@Test
	public void testarErroDelecaoUsuario() {
		boolean usuarioDelecao = usuarioService.deletarUsuario(usuario.getId());
		
		assertFalse(usuarioDelecao);
	}


	@Test
	public void testarDeletarUsuario() {
		Mockito.when(usuarioRepository.findById(1)).thenReturn(Optional.of(usuario));
		
		boolean usuarioDeletado = usuarioService.deletarUsuario(usuario.getId());
		
		assertTrue(usuarioDeletado);
	}
	
	@Test
	public void testarBuscarUsuarios() {
		Mockito.when(usuarioRepository.findAll()).thenReturn(usuarios);
		
		ArrayList<Usuario> buscarUsuarios = (ArrayList<Usuario>) usuarioService.obterUsuario();
		
		Assert.assertEquals(1, buscarUsuarios.size());
		Assert.assertEquals("teste", buscarUsuarios.get(0).getNome());
		
	}
	
	@Test
	public void testarBuscarUmUsuario() {
		Mockito.when(usuarioRepository.findAllByNome("teste")).thenReturn(usuarios);	
		
		ArrayList<Usuario> buscarUsuario = (ArrayList<Usuario>) usuarioService.obterUsuarioPorNome(usuario.getNome());
		
		Assert.assertEquals("teste", buscarUsuario.get(0).getNome());	

	}
}
