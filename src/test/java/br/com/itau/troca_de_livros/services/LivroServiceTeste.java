package br.com.itau.troca_de_livros.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import br.com.itau.troca_de_livros.models.Autor;
import br.com.itau.troca_de_livros.models.Categoria;
import br.com.itau.troca_de_livros.models.Livro;
import br.com.itau.troca_de_livros.models.Usuario;
import br.com.itau.troca_de_livros.repositories.AutorRepository;
import br.com.itau.troca_de_livros.repositories.LivroRepository;

public class LivroServiceTeste {
	
	Livro livro;
	Autor autor;
	Usuario usuario;
	Categoria categoria;
	LivroService livroService;
	AutorService autorService;
	LivroRepository livroRepository;
	AutorRepository autorRepository;
	
	ArrayList<Livro> livros;

	@Before
	public void inicializar() {
		livroService = new LivroService();
		livroRepository = Mockito.mock(LivroRepository.class);
		livroService.livroRepository = livroRepository;
		livro = new Livro();
		autor = new Autor();
		livros = new ArrayList<>();
		livros.add(livro);
		livro.setId(1);;
		livro.setTitulo("Cinquenta Tons de Cinza");
		livro.setAutor(autor);
		livro.setCategoria(categoria);
		livro.setDisponivel(1);
		livro.setUsuario(usuario);
		autor.setId(1);
		autor.setNome("Erika Leonard James");
	}
	
	@Test
	public void testarInsercaoLivro() {
		Mockito.when(livroRepository.save(livro)).thenReturn(livro);
		
		boolean novoLivro = livroService.inserir(livro);
		
		assertTrue(novoLivro);

	}
	
	@Test
	public void testarErroAlteracaoLivro() {
		Mockito.when(livroRepository.save(livro)).thenReturn(livro);
		
		boolean livroNaoAlterado = livroService.alterar(livro.getId(), livro);
		
		assertFalse(livroNaoAlterado);
	}
	
	
	@Test
	public void testarAlteracaoLivro() {
		Mockito.when(livroRepository.save(livro)).thenReturn(livro);
		Mockito.when(livroRepository.findById(1)).thenReturn(Optional.of(livro));
		
		boolean livroAlterado = livroService.alterar(livro.getId(), livro);
		
		assertTrue(livroAlterado);
	}
	
	@Test
	public void testarAlteracaoLivro2() {
		Mockito.when(livroRepository.save(livro)).thenReturn(livro);
		Mockito.when(livroRepository.findById(1)).thenReturn(Optional.of(livro));
		
		boolean livroAlterado = livroService.alterar(livro.getId(), livro);
		
		assertTrue(livroAlterado);
	}
	
	@Test
	public void testarErroDelecaoLivro() {
		boolean livroNaoDeletado = livroService.deletarLivro(livro.getId());
		
		assertFalse(livroNaoDeletado);
	}
	
	@Test
	public void testarDelecaoLivro() {
		Mockito.when(livroRepository.findById(1)).thenReturn(Optional.of(livro));
		
		boolean livroDeletado = livroService.deletarLivro(livro.getId());
		
		assertTrue(livroDeletado);
	}

	@Test
	public void testarBuscarLivros() {
		Mockito.when(livroRepository.findAll()).thenReturn(livros);
		
		ArrayList<Livro> buscarLivros = (ArrayList<Livro>) livroService.buscarTodos();
		
		assertEquals(1, buscarLivros.size());
		assertEquals("Cinquenta Tons de Cinza", buscarLivros.get(0).getTitulo());
		
	}
	
	@Test
	public void testarBuscarUmLivroPorId() {
		Mockito.when(livroRepository.findById(1)).thenReturn(Optional.of(livro));
		
		Optional<Livro> buscarLivro = livroService.obterLivroPorId(livro.getId());
		
		assertEquals("Cinquenta Tons de Cinza", buscarLivro.get().getTitulo());

	}
	
	@Test
	public void testarBuscarUmLivro() {
		Mockito.when(livroRepository.findAllByTitulo("Cinquenta Tons de Cinza")).thenReturn(livros);
		
		ArrayList<Livro> buscarLivro = (ArrayList<Livro>) livroService.obterLivroPorTitulo(livro.getTitulo());
		
		assertEquals(1, buscarLivro.size());
		assertEquals("Cinquenta Tons de Cinza", buscarLivro.get(0).getTitulo());

	}
}
