package br.com.itau.troca_de_livros.services;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import br.com.itau.troca_de_livros.inout.TrocaEntrada;
import br.com.itau.troca_de_livros.models.Livro;
import br.com.itau.troca_de_livros.models.Usuario;
import br.com.itau.troca_de_livros.repositories.LivroRepository;
import br.com.itau.troca_de_livros.repositories.UsuarioRepository;

public class TrocaServiceTeste {

	Livro livro;
	Usuario usuario;
	TrocaEntrada trocaEntrada;
	TrocaService trocaService;
	LivroRepository livroRepository;
	UsuarioRepository usuarioRepository;
	
	
	@Before
	public void inicializar() {
		trocaService = new TrocaService();
		livroRepository = Mockito.mock(LivroRepository.class);
		usuarioRepository = Mockito.mock(UsuarioRepository.class);
		trocaService.livroRepository = livroRepository;
		trocaService.usuarioRepository = usuarioRepository;
		livro = new Livro();
		trocaEntrada = new TrocaEntrada();
		usuario = new Usuario();
		livro.setId(1);
		livro.setId(2);
		usuario.setId(3);
		usuario.setId(4);
		trocaEntrada.setIdEnviado(1);
		trocaEntrada.setIdSolicitado(2);
		trocaEntrada.setUsuario_idEnviado(3);
		trocaEntrada.setUsuario_idSolicitado(4);
	}
		
	@Test
	public void testarAlteracaoLivro() {
		Mockito.when(livroRepository.save(livro)).thenReturn(livro);
		Mockito.when(usuarioRepository.save(usuario)).thenReturn(usuario);
		Mockito.when(livroRepository.findById(1)).thenReturn(Optional.of(livro));
		Mockito.when(livroRepository.findById(2)).thenReturn(Optional.of(livro));
		Mockito.when(usuarioRepository.findById(3)).thenReturn(Optional.of(usuario));
		Mockito.when(usuarioRepository.findById(4)).thenReturn(Optional.of(usuario));
		
		boolean livrosTrocados = trocaService.trocarLivros(trocaEntrada);
		
		assertTrue(livrosTrocados);
	}
	
	@Test
	public void testarErroAlteracaoLivro() {
		trocaEntrada.setUsuario_idEnviado(1);
		trocaEntrada.setUsuario_idSolicitado(1);
	
		Mockito.when(livroRepository.save(livro)).thenReturn(livro);
		Mockito.when(usuarioRepository.save(usuario)).thenReturn(usuario);
		Mockito.when(livroRepository.findById(1)).thenReturn(Optional.of(livro));
		Mockito.when(livroRepository.findById(2)).thenReturn(Optional.of(livro));
		
		boolean livrosTrocados = trocaService.trocarLivros(trocaEntrada);
		
		assertFalse(livrosTrocados);
	}
}
